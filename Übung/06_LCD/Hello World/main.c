/*
 * LCD_Demo.c
 *
 * Created: 09.12.2021 13:19:15
 * Author : Sebastian
 */ 


#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include "lcd.h"
#include "avr/interrupt.h"

volatile uint8_t counter;
volatile uint8_t running;

void LCD_Demo();

ISR(INT0_vect) {
	running = 0;
}
ISR(INT1_vect) {
	running = 1;
	counter = 0;
}


int main(void)
{
	
	cli();
	lcd_init(LCD_DISP_ON);
	sei();
	PORTD |= (1<<PORTD2) | (1<<PORTD3);
	EIMSK |= (1<<INT0) | (1<<INT1);
	
	lcd_clrscr();
	char buffer[32];
	running = 1;
	while (1)
	{
		if(running) {
			counter = counter + 1;
			sprintf(buffer, "Dauer: %usek.-r", counter);
			_delay_ms(1000);
			lcd_clrscr();
			lcd_puts(buffer);
			} else {
			sprintf(buffer, "Dauer: %usek.-s", counter);
			_delay_ms(1000);
			lcd_clrscr();
			lcd_puts(buffer);
		}
	}
	
}


void LCD_Demo(){
	// init
	lcd_init(LCD_DISP_ON);
	
	
	//einzelnes Zeichen ausgeben
	lcd_putc('A');
	_delay_ms(1000);
	lcd_clrscr();
	
	//String ausgeben
	lcd_puts("Hello world!");
	_delay_ms(1000);
	
	
	/* Replace with your application code */
	while (1)
	{
		for (int i = 0; i < 17; i++)
		{
			lcd_putc(' ');
		}
		lcd_puts("Hello World");
		_delay_ms(1000);
	}
}


