/*
 * LCD.c
 *
 * Created: 16.12.2021 15:52:42
 * Author : Sebastian
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd.h"

#define U_REF 5.0
#define ADC_MAX_RES 1024.0
volatile result=0;
uint8_t buffer[32];


int main(void)
{

// wenn wir analogen Eingang A0 verwenden, sind keine MUX-Bits zu setzen
ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);

// ADEN: ADC-Komponente aktivieren
// ADPS0-2: Den Takt reduzieren auf 200 kHz
ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);

sei;

lcd_init(LCD_DISP_ON);



	
	while (1)
	{	
	}
}
// ISR ausgeführt wenn Messung abgeschlossen
ISR(ADC_vect){
	result = ADCW;
	
	// Ergebnis in String konvertieren
	sprintf(buffer, "Spg: %.2fV", result);
	lcd_clrscr();
	lcd_puts(buffer);
	_delay_ms(1000);
	ADCSRA |= (1<<ADSC);
}
