/*
 * LCD.c Interrupr basierter Free Running Mode
 *
 * Created: 16.12.2021 15:52:42
 * Author : Sebastian
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>

#define U_REF 5.0
#define ADC_MAX_RES 1024.0

//Das ADC-Wandlungsergebnis liegt zw. 0 - 1023, daher 2 Bytes (16 Bits)
volatile uint16_t result = 0;
volatile uint8_t buffer[32];
volatile float voltage;
int main(void)
{
	//Nachdem wir den analogen Eingang A0 verwenden, sind kene MUX-Bits zu setzen (Default=A0)
	//ADMUX |= (1<<REFS0); // | (1<<MUX0) | (1<<MUX1);
	
	//Wir wollen aber am A3 den analogen Eingang verwenden und m�ssen somit die MUX-Bits setzen
	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);
	
	//ADEN: ADC-Komponente aktivieren
	//ADIE: ADC-Interrupt aktivieren
	//ADATE: Auto Trigger Mode aktivieren -> Free Running Mode aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADATE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	// Free Running ist im ADCSRB zu "konfigurieren" => Default erledigt: ADTS0 - ADTS2 sind "0"!
	
	sei();
	
	lcd_init(LCD_DISP_ON);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
	/* Replace with your application code */
	while (1);
}
// ISR ausgef�hrt wenn Messung abgeschlossen
ISR(ADC_vect){
	//Das Ergebnis
	result = ADCW;
	voltage = (ADCW/ADC_MAX_RES)*U_REF;
	
	if (voltage > 4.0)
	{
		PORTD = 0x00;
		PORTD |= (1<<PORTD1);
	}else if(voltage < 2.0){
		PORTD = 0x00;
		PORTD = (1<<PORTD2);
	}else{
		PORTD = 0x00;
		PORTD = (1<<PORTD3);
	}
	
	//Ergebnis in einen String konvertieren
	sprintf(buffer, "Spg: %.2fV", voltage);
	
	lcd_clrscr();
	lcd_puts(buffer);
	
	_delay_ms(250);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
	
}