/*
 * LCD.c
 *
 * Created: 16.12.2021 15:52:42
 * Author : Sebastian
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"

#define U_REF 5.0
#define ADC_MAX_RES 1024.0


int main(void)
{
	// ADC-Ergebnis liegt zweischen 0 und 1023, 2 Bytes
	float result;
	uint8_t buffer[32];
	
	// wenn wir analogen Eingang A0 verwenden, sind keine MUX-Bits zu setzen
	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);
	
	// ADEN: ADC-Komponente aktivieren
	// ADPS0-2: Den Takt reduzieren auf 200 kHz
	ADCSRA |= (1<<ADEN) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	lcd_init(LCD_DISP_ON);
	
	/* Replace with your application code */
	while (1)
	{
		// Messung starten
		ADCSRA |= (1<<ADSC);
		
		// Feststellen ob Messung fertig ist: solange ADSC 1 ist l�uft Messung, wird bei Abschluss automatisch auf null gesetzt
		while(ADCSRA & (1<<ADSC));
		
		// Ergebnis
		result = (ADCW / ADC_MAX_RES) * U_REF;
		
		// Ergebnis in String konvertieren
		sprintf(buffer, "Spg: %.2fV", result);
		lcd_clrscr();
		lcd_puts(buffer);
		_delay_ms(1000);
	}
}