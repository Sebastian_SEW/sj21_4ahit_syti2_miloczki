/*
 * Test_Beispiel.c
 *
 * Created: 21.11.2021 11:34:28
 * Author : Sebastian
 */ 

#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>

void init();

int main() {
	
	PORTC != (1<<PORTC2) // Pull up
	
	while(1) {
		if(PIND & (1<<PIND2)) { // geht auch mit Bit/Byte Toggler
			if(PORTD) {
				PORTD &= (1<<PORTD0);
			}
			else {
				PORTD &= PORTD0;
				PORTD |= (1<<PORTD0) | (1<<PORTD1);
			}
			if(PORTD & ((1<<PORTD6) | (1<<PORTD7))) {
				PORTD |= 0xFF;
				_delay_ms(1000);
				PORTD &= ((1<<PORTD0) | (1<<PORTD1));
			}
			_delay_ms(500);
			PORTD = (PORTD<<2);
		}
	}
}
void init() {
	DDRD |= 0xFF; // LEDs am PORTD als Ausgang konfigurieren
	DDRC &= ~(1<<DDC2); // PIN2 am PORTC als Eingang konfigurieren
	PORTD |= (1<<PORTD0) | (1<<PORTD1); // erste 2 LEDs einschalten
}