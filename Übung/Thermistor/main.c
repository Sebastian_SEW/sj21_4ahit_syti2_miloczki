/*
 * Joystick.c
 *
 * Created: 27.01.2022 14:31:14
 * Author : Sebastian
 */#define F_CPU 16000000UL

 #include <avr/io.h>
 #include <avr/interrupt.h>
 #include "lcd.h"
 #include <util/delay.h>
 #include <math.h>

#define U_REF 5.0
#define ADC_MAX_RES 1024.0

//Das ADC-Wandlungsergebnis liegt zw. 0 - 1023, daher 2 Bytes (16 Bits)

volatile uint8_t buffer[32];
volatile float voltage;
volatile float result = 0;
int main(void)
{
	//Nachdem wir den analogen Eingang A0 verwenden, sind kene MUX-Bits zu setzen (Default=A0)
	//ADMUX |= (1<<REFS0); // | (1<<MUX0) | (1<<MUX1);
	
	//Wir wollen aber am A3 den analogen Eingang verwenden und m�ssen somit die MUX-Bits setzen
	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);
	
	//ADEN: ADC-Komponente aktivieren
	//ADIE: ADC-Interrupt aktivieren
	//ADATE: Auto Trigger Mode aktivieren -> Free Running Mode aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADATE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	// Free Running ist im ADCSRB zu "konfigurieren" => Default erledigt: ADTS0 - ADTS2 sind "0"!
	
	sei();
	
	lcd_init(LCD_DISP_ON);

	ADCSRA |= (1<<ADSC);

	while (1);
}

ISR(ADC_vect){
	
	result = ADCW;
	
	 double tempK = log(10000.0 * ((1024.0 / ADCW - 1)));
	 tempK = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * tempK * tempK)) * tempK);
	 float tempC = tempK - 273.15;
	
	
	sprintf(buffer, "T: %.2fC", tempC);
	
	lcd_clrscr();
	lcd_puts(buffer);
	
	_delay_ms(1000);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
	
}