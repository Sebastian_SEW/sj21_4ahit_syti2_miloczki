/*
 * 07_A_Timer.c
 *
 * Created: 24.02.2022 13:48:13
 * Author : Sebastian
 */ 

// LED die im Sekunden Takt blinkt am PB0, Timer basiert

#define F_CPU 16000000UL


#include <avr/io.h>
#include <avr/interrupt.h>

// 1/F = 1/16M = 0,0000625ms 

int main(void)
{
	// Timer im Normal Mode
	
	// Pin PB0 als AUsgang konfiguriert
	DDRD |= (1<<DDD2);
	// vorladen --> Timer
	TCNT1 = 3.035;
	// Timer mit System-Takt (16Mhz) aktivieren
    TCCR1B |= (1<<CS12);
	
	// Timer Overflow Interrupt aktivieren TIMSK1
	TIMSK1 |= (1<<TOIE1);
	
	//Interrupts global aktiveiren
	sei();
	
	while (1) 
    {}
}

ISR(TIMER1_OVF_vect){
	PORTD ^= (1<<PORTD2);
	TCNT1 = 3.035;
}

