/*
 * Digital_Inputs.c
 *
 * Created: 21.10.2021 14:09:10
 * Author : Sebastian
 */ 


#define F_CPU 16000000 //16 MHz

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

int boolean = 1;
void pushButton();
void switchLED();
void button0();
void button1();
void pollButtons();
void highEndRunningLED();
void Buttons();
void activeLow();

int d = 2;


int main(void)
{
	DDRD = 0xff;
	PORTD = (1<<PORTD0);
	PORTB |= (1<<PORTB0)|(1<<PORTB1)|(1<< PORTB2);
	
	while (1){
		Buttons();
		activeLow();
	}
}


void pushButton(){
	 if(PIND & (1<<PIND2))
		PORTD |= (1<<PORTD7);
	 else
		PORTD &= ~(1<<PORTD7);
}
void switchLED(){
	extern int boolean;
	
	if(PIND & (1<<PIND2)){
	if(boolean){
	boolean ^= 1;
	PORTD |= (1<<PORTD7);}
	else
	PORTD &= ~(1<<PORTD7);}
	
}




void button0(){
	if(PIND & (1<<PIND2))
	PORTD |= (1<<PORTD7);
	else
	PORTD &= ~(1<<PORTD7);
}

void button1(){
	_delay_ms(200);
	if(PIND & (1<<PIND2)){
		PORTD = ~PORTD7;
	}
}


void pollButtons(){
	if(PINC & (1<<PINC5)){
		d = 30;
	}
	if(PINC & (1<<PINC4)){
		d = 20;
	}
	if(PINC & (1<<PINC3)){
		d = 10;
	}
}
// ! invertiert Byte-weise ~ invertiert Bit-weise

void highEndRunningLED(){
	for (int i = d;i > 0;i--){
		_delay_ms(100);
	}
	if (PORTD & (1<<PORTD7)){
		PORTD = (1<<PORTD0);
	}
	else{
		PORTD <<= 1;
	}
}

// Active Low:
// Variante 1: externer Widerstand �ber Schalter platziert, Taste nicht bet�tigt: 5V, bet�tigt: 0V -> Umkehr der if-Abfragen -> auf 0 abfragen
// ! = byteweises Invertieren, ganzes Byte wird invertiert, 00000001 wird zu 00000000
// Variante 2: interner Pull-Up-Widerstand, in Pins aktivierbarer Widerst�nde eingebaut, werden �ber Portregister mit Pipe aktiviert ->
// PORTC|=(1<<PINC0); einmaliger Config



void Buttons(){
	if(!(PINB & (1<<PINB0))){
		d = 1;
	}
	if(!(PINB & (1<<PINB1))){
		d = 4;
	}
	if(!(PINB & (1<<PINB2))){
		d = 20;
	}
}

void activeLow(){
	extern int d;
	for (int i = d;i > 0;i--){
		_delay_ms(100);
	}
	if (PORTD & (1<<PORTD7)){
		PORTD = (1<<PORTD0);
	}
	else{
		PORTD <<= 1;
	}
}



ISR(INT0_vect){
	
}






