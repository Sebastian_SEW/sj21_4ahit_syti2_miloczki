/*
 * Joystick.c
 *
 * Created: 27.01.2022 14:31:14
 * Author : Sebastian
 */
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>

volatile uint8_t x = 7;
volatile uint8_t y = 0;

void print_position();

int main(void)
{

    ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX2); 

    ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADATE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
    

    
    sei();
    
    lcd_init(LCD_DISP_ON);
    
    //Messung starten
    ADCSRA |= (1<<ADSC);
    while (1);
}

void print_position() {
    lcd_clrscr();
    
    lcd_gotoxy(x, y);
    lcd_putc('X');
}

ISR(ADC_vect){
    uint16_t temp = 0;
    //A1 (Y) 
    if(ADMUX & (1<<MUX2)) {
        temp = ADCW;
        if(temp > 600) {
            y = 1;
        } else if(temp < 300) {
            y = 0;
        }
        ADMUX &= ~(1<<MUX2);
        ADMUX |= (1<<MUX0) | (1<<MUX1);
    } 
    //A0 (X) 
    else {
        temp = ADCW;
        if(temp > 600 && x <= 14) {
            x++;
        } else if(temp < 500 && x > 0) {
            x--;
        }
        ADMUX |= (1<<MUX2);
        ADMUX &= ~((1<<MUX0) | (1<<MUX1));
    }
    print_position();
    _delay_ms(250);
}