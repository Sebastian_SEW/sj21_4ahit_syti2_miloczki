/*
 * Interrupts.c
 *
 * 3 Tasten am PORTC active low mit interen pull up widerstand
 * Created: 25.11.2021 14:12:09
 * Author : Sebastian
 */ 

#define F_CPU 16000000 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

int d = 1;
int running = 1;

void Interrupt();
void init();
void init_interrupt();
void delay_interrupt();

int main(void){
	
	init_interrupt();
	while (1)
	{
		if(running == 1)
		{
			for(int i = d; i > 0; i--) {
				_delay_ms(100);
			}
			PORTD = (PORTD<<1);
			if(PORTD == (1<<PORTD6)) {
				for(int i = 0; i < 6; i++) {
					if(running == 1){
						for(int i = d; i > 0; i--) {
							_delay_ms(100);
						}
						PORTD = (PORTD>>1);
					}
				}
			}
		}
	}
}

void init_interrupt(){
	// LEDs
	DDRD |= (1<<DDD0) | (1<<DDD1) | (1<<DDD2) | (1<<DDD3) | (1<<DDD4) | (1<<DDD5) | (1<<DDD6);
	PORTD |= (1<<PORTD0);
	
	cli();
	// Interne PU-Widerst�nde aktivieren
	PORTC |= (1<<PORTC0) | (1<<PORTC1) | (1<<PORTC2);
	// Externe Interrupt-Gruppe f�r Port C aktivieren
	PCICR |= (1<<PCIE1);
	// einzelne Interrupts auf Pin-Ebene aktivieren
	PCMSK1 |= (1<<PCINT10) | (1<<PCINT9) | (1<<PCINT8);
	sei();
}

ISR(PCINT1_vect){
	//Prellen!
	if(!(PINC & (1<<PINC0))){
		PORTD &= ~(1<<PORTD);
		PORTD |= (1<<PORTD0);
		running = 1;
	}
	else if(!(PINC & (1<<PINC1))){
		running = 0;
	}
	else if(!(PINC & (1<<PINC2))){
		delay_interrupt();
	}
}

void delay_interrupt(){
	if(d == 1){
		d++;
	}
	else if(d == 2){
		d++;
	}
	else if(d == 3){
		d = 1;
	}
}



void Interrupt(){
	cli();// SREG das Pin7 l�scht  (SREG = Status Register --> siehe Datenblatt)
	// LED am PD7
	DDRD |= (1<<DDD7);
	// Tasten am PD3 und PD2
	PORTD |= (1<<PORTD2) | (1<<PORTD3);
	
	// Konfiguration von INT0 und INT1
	EIMSK |= (1<<INT0) | (1<<INT1);
	
	sei(); // SREG das Pin7 setzt
}

// f�r PD2
// LED am PD7 einschalten
//ISR(INT0_vect){
//	PORTD != (1<<PORTD7);
//}


// f�r PD3
// LED am PD7 ausschalten
//ISR(INT1_vect){
//	PORTD &= ~(1<<PORTD7);
//}





