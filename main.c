/*
 * 02_PortAccess.c
 *
 * Created: 30.09.2021 14:20:47
 * Author : Sebastian
 */ 

#define F_CPU 16000000 //16 MHz

#include <avr/io.h>
#include <util/delay.h>

//Definition des Funktionsprototypen
//void lauflicht0();
//void lauflicht1();	
//void lauflicht2();
void lauflicht3();

	
int main(void)
{
	// Wir definieren das Pin 0 vom Port C (kurz PC0) als Ausgang
	DDRD=0xFF;
	PORTD = 0xF0;
	//Arbeitsschleife
	while (1){
		lauflicht3();

	}
}


void lauflicht0(){
	// Schalten wir die LED PC0 ein => bewirkt 5V an diesem Pin!
	PORTD=0xF1;
	_delay_ms(500);
	PORTD=0xF2;
	_delay_ms(500);
	PORTD=0xF4;
	_delay_ms(500);
	PORTD=0xF8;
	_delay_ms(500);
	
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

void lauflicht1(){
	PORTD |= 0x01;
	_delay_ms(500);
	PORTD &= 0xFE;
	PORTD |= 0x02;
	_delay_ms(500);
	PORTD &= 0xFD; // PORTD &= ~0x02
	PORTD |= 0x04;
	_delay_ms(500);
	PORTD &= 0xFB;
	PORTD |= 0x08;
	_delay_ms(500);
	PORTD &= 0xF7;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/

void lauflicht2(){
		PORTD |= 0x01; 
		_delay_ms(500); 
		PORTD &= ~0x01;// MIt dem ~ Operator kann man die Bitmaske invertieren ~0x01 --> 0xFE
		PORTD |= 0x02;
		_delay_ms(500);
		PORTD &= ~0x02; 
		PORTD |= 0x04;
		_delay_ms(500);
		PORTD &= ~0x04;
		PORTD |= 0x08;
		_delay_ms(500);
		PORTD &= ~0x08;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

void lauflicht3(){
		PORTD |= (1<<0);
		_delay_ms(500);
		PORTD &= ~(1<<0);
		PORTD |= (1<<1);
		_delay_ms(500);
		PORTD &= ~(1<<1);
		PORTD |= (1<<2);
		_delay_ms(500);
		PORTD &= ~(1<<2);
		PORTD |= (1<<3);
		_delay_ms(500);
		PORTD &= ~(1<<3);
}

